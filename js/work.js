const Work = {
    pay: 0,
    work: function() {
        Work.pay += 100;
        render();
    },
    bank: function(){
        if(Work.pay === 0){
            alert('Nothing to send to the bank')
            return;
        }
        
        Bank.balance += Number(Work.pay);
        Work.pay = 0;
        render();      
    }
}

const initStartPay = (pay) => Work.pay = pay;