const createBoughtLaptop = (laptop) => {
    const section = document.createElement('section');
    const cardBody = document.createElement('div');
    const h4Name = document.createElement('h4');
    const img = document.createElement('img');
    
    section.className = "card bg-light mb-3 size-25";
    cardBody.className = "card-body";

    h4Name.innerText = laptop.name;
    h4Name.className = 'card-title';
    img.src = laptop.image;
    img.className = 'size-100';
    cardBody.appendChild(h4Name)
    cardBody.appendChild(img);
    section.appendChild(cardBody);

    return section;
};