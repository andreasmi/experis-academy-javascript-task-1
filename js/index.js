
//Variables
const startBalance = 100;
const startPay = 0;
const boughtLaptops = [];
const dropDown = document.getElementById('laptops');
let selectedLaptopIdx = 0;

// DOM elements
const btnGetLoan = document.getElementById('btnGetLoan');
const btnWork = document.getElementById('btnWork');
const btnBank = document.getElementById('btnBank');
const btnBuy = document.getElementById('btnBuyLaptop');
const elWorkPay = document.getElementById('workPay');
const elBankBalance = document.getElementById('bankBalance');
const elDropDownList = document.getElementById('laptops');
const elFeatureList = document.getElementById('featureList');
const elDescName = document.getElementById('laptopName');
const elDescDesc = document.getElementById('laptopDesc');
const elDescImg = document.getElementById('laptopImg');
const elDescPrice = document.getElementById('laptopPrice');
const elBoughtLaptops = document.getElementById('boughtLaptops');
const elBoughtLaptopsHeading = document.getElementById('boughtLaptopsHeading');

const getSelectedLaptopIdx = () => dropDown.value;

function populateDropDown(){
    for (let i = 0; i < laptops.length; i++) {
        const element = laptops[i];
        let opt = document.createElement('option');
        opt.text = element.name;
        opt.value = i;
        dropDown.appendChild(opt);
    }
}

function changeLaptop(){
    selectedLaptopIdx = getSelectedLaptopIdx();

    elFeatureList.innerHTML = '';
    let featureListOfLaptop = laptops[selectedLaptopIdx].featureList;

    for(let i = 0; i < featureListOfLaptop.length; i++){
        let li = document.createElement('li');
        li.innerText = featureListOfLaptop[i];
        li.className = "list-group-item list-group-item-info";
        elFeatureList.appendChild(li);
    }

    elDescName.innerText = laptops[selectedLaptopIdx].name;
    elDescDesc.innerText = laptops[selectedLaptopIdx].description;
    elDescPrice.innerText = laptops[selectedLaptopIdx].price;
    elDescImg.src = laptops[selectedLaptopIdx].image;
    elDescImg.alt = 'Showing laptop: ' + laptops[selectedLaptopIdx].name;
}


function buyLaptop(){
    const laptopPrice = laptops[selectedLaptopIdx].price;
    if(Bank.balance < Number(laptopPrice)){
        alert('You do not have enought money to buy this computer');
        return;
    }

    Bank.balance -= laptopPrice;
    render();
    boughtLaptops.push(selectedLaptopIdx);
    Bank.allowLoan = true;

    elBoughtLaptops.appendChild(createBoughtLaptop(laptops[selectedLaptopIdx]));
    
    elBoughtLaptopsHeading.className = '';
}

const render = () => {
    document.getElementById('workPay').innerHTML = Work.pay;
    document.getElementById('bankBalance').innerHTML = Bank.balance;
}

//Function for initializing
(function(){
    initLaptops();
    populateDropDown();
    changeLaptop();

    initBankBalance(startBalance);
    initStartPay(startPay);

    render();
})();

btnGetLoan.addEventListener('click', Bank.getLoan);
btnWork.addEventListener('click', Work.work);
btnBank.addEventListener('click', Work.bank);
btnBuy.addEventListener('click', buyLaptop);
elDropDownList.addEventListener('click', changeLaptop);
