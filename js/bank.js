const numbers = /^[0-9]+$/;
const Bank = {
    name: '',
    balance: 0,
    allowLoan: true,
    getLoan: function() {
        if(!Bank.allowLoan){
            alert('You must buy a computer before applying for another loan');
            return;
        }

        let loanSum = prompt("Please enter an amount", 0);
        if(loanSum.match(numbers)){
            let allowedLoan =  parseInt(Bank.balance * 2);
            if(parseInt(loanSum) > allowedLoan){
                alert('You do not have enough balance to get this loan');
                return;
            }

            Bank.balance += parseInt(loanSum);
            Bank.allowLoan = false;
            render();
            
        }else{
            alert('Only numbers allowed');
        }
    }
}

const initBankName = (name) => Bank.name = name;
const initBankBalance = (balance) => Bank.balance = balance;