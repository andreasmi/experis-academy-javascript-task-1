//Laptop handling

const laptops = [];
function Laptop(name, description, price, featureList, image){
    this.name = name;
    this.description = description;
    this.price = price;
    this.featureList = featureList;
    this.image = image;
}

const generateLaptop = (name, description, price, featureList, image) => 
    laptops.push(new Laptop(name, description, price, featureList, image));

const initLaptops = () => {
    //Thank you komplett.no for all the images :)
    generateLaptop('Acer Nitro 5', 'Whatever you try to do, the screen is orange. It has some buttons you can press if you want.. Only if you need them tho, we doubt you really do.', 3400, ['Some buttons', 'Orange screen', 'Ridicuolous power usage'], 'res/laptopImages/AcerNitro.jpg'); //https://www.komplett.no/product/1157681/gaming/gaming-pc/baerbar/acer-nitro-5-an515-55-156-fhd-144-hz#
    generateLaptop('Asus Rog', 'This is a colorfull laptop. It has a sky as it\'s background image! The power button is kinda hard to find, but when you first do find it... You\'ll be amazed by what happens next! Ohh.. You also need to find the mysterious power supply', 1200, ['Mousepad', 'Camera', 'Camera cover'], 'res/laptopImages/AsusRog.jpg'); //https://www.komplett.no/product/1155434/gaming/gaming-pc/baerbar/asus-rog-zephyrus-duo-gx550lxs-156-fhd-300-hz-g-sync
    generateLaptop('Lenovo V15', 'As you can see, it clearly has windows 10. It also only comes with the Norsk språk pakke... Det kan fort bli ett problem om man ikke er så god i norsk.', 1200, ['Nitrus mode', 'Power button', 'Screen'], 'res/laptopImages/LenovoV15.jpg'); //https://www.komplett.no/product/1155228/pc-nettbrett/pc-baerbar-laptop/alle-baerbare-pc-er/lenovo-v15-156-fhd?gclid=CjwKCAjw4MP5BRBtEiwASfwAL9jN_iwlKpjiNqwVSpwP7Vm0456P3JdYrAzWdn1NacZlwdJ25SbVABoCYlEQAvD_BwE&gclsrc=aw.ds
    generateLaptop('MSI GF63', 'This laptop is actually on sale!!! Who doesn\'t like that? It usually cost 20000 nok but the store needs a bit more money.. Soooooo, It\'s on a sale that benefits the store. ', 45000, ['ON SALE', 'Reliable (we think)', 'SD-card slot'], 'res/laptopImages/MsiGF63.jpg'); //https://www.komplett.no/product/1153473/gaming/gaming-pc/baerbar/msi-gf63-156-fhd-120-hz

}
