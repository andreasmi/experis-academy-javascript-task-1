# Komputer Store
Experis Academy - JavaScript Task 1

## Description
This webpage is a mochup online computer store with some simple functionality written in JavaScript. The page is divided into three main parts:

### Bank:
The bank is where you store your money. There is a button here called "Get a loan" which you can press to apply for a loan. You can only get one loan before you buy a computer. You could get a new loan after you have bought a computer. The amaount of money you can loan is no more than the double of what you have in the bank balance.

### Work:
The work section is where you could get more money. Here there are two buttons, work and bank. The work button increases your pay with 100 kr every time you press it, and the bank button sends the pay to the bank.

### Laptops:
The laptop section is divided into three parts. You have one part with a dropdown list where you could select a new laptop to be viewed. The next part is the the section where you can view the laptops. This sections contains a title, description and an image of the laptop. At the footer there is a list of laptops the user already has bought.

## Usage

Clone/Fork repository and launch the index.html file.